# Counter-Strike: Global Offensive Dedicated Server Dockerfile
This repository contains **Dockerfile** of [Linux Game Server Manager's CS:GO Server](http://gameservermanagers.com/lgsm/csgoserver/) for [Docker](https://www.docker.com/) published to the public [Docker Hub Registry](https://registry.hub.docker.com/).
Also see [johnjelinek/csgoserver](https://registry.hub.docker.com/u/johnjelinek/csgoserver/)

### Base Docker Image
* [debian:stretch-slim](https://hub.docker.com/_/debian/)

### Deployment
1. Docker Compose file of this repo.
2. Download the [image](https://registry.gitlab.com/ejectedspace/docker-csgo) from this repos registery: `docker pull registry.gitlab.com/ejectedspace/docker-csgo` and run it manually

#### Setup
Before using the compose file to start the server you need to install it into a volume:
```
docker run -v csgo:/csgoserver/server -it registry.gitlab.com/ejectedspace/docker-csgo bash -c /csgoserver/install
```

You should now review the `docker-compose.yml` file and add you Server Token.

#### Running
Now you can use the compose file with `docker-compose up -d` to start the server.

Open CS:GO and Browse Community Servers. Open `yourServerIP` to see the result.
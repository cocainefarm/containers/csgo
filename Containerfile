#
# CSGO Dockerfile
#
# https://hub.docker.com/r/austinsaintaubin/docker-csgoserver/
# Also see: https://hub.docker.com/r/johnjelinek/csgoserver/~/dockerfile/

# Pull the base image
FROM debian:buster-slim
MAINTAINER Max Audron <audron@ejected.space>

ENV DEBIAN_FRONTEND noninteractive

#### Variables ####
ENV SERVER_NAME "Counter Strike: Global Offensive - Docker Server"
ENV RCON_PASS rconpass
ENV SERVER_PASS ""
ENV SERVER_LAN 0
ENV SERVER_REGION 0

# Notification Email
# (on|off)
ENV EMAIL_NOTIFICATION off
ENV EMAIL email@example.com

# STEAM LOGIN
ENV STEAM_USER anonymous
ENV STEAM_PASS ""

# Start Variables
# https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Dedicated_Servers#Starting_the_Server
# [Game Modes]           gametype    gamemode
# Arms Race                  1            0
# Classic Casual             0            0
# Classic Competitive        0            1
# Demolition                 1            1
# Deathmatch                 1            2
ENV GAME_MODE 0
ENV GAME_TYPE 0
ENV DEFAULT_MAP de_dust2
ENV MAP_GROUP random_classic
ENV MAX_PLAYERS 16
ENV TICK_RATE 64
ENV GAME_PORT 27015
ENV SOURCE_TV_PORT 27020
ENV CLIENT_PORT 27005
ENV GAME_IP 0.0.0.0
ENV PUBLIC_IP 0.0.0.0
ENV SERVER_TOKEN ""

# Optional: Workshop Parameters
# https://developer.valvesoftware.com/wiki/CSGO_Workshop_For_Server_Operators
# To get an authkey visit - http://steamcommunity.com/dev/apikey
ENV AUTH_KEY ""
ENV WS_COLLECTION_ID ""
ENV WS_START_MAP ""

# Expose Ports
EXPOSE $GAME_PORT
EXPOSE $GAME_PORT/udp
EXPOSE $SOURCE_TV_PORT/udp
EXPOSE $CLIENT_PORT/udp
#EXPOSE 1200/udp

RUN echo 'deb http://deb.debian.org/debian stretch main contrib non-free' >> /etc/apt/sources.list && \
    echo 'deb-src http://deb.debian.org/debian stretch main contrib non-free' >> /etc/apt/sources.list && \
    echo 'deb http://deb.debian.org/debian stretch-updates main contrib non-free' >> /etc/apt/sources.list && \
    echo 'deb-src http://deb.debian.org/debian stretch-updates main contrib non-free' >> /etc/apt/sources.list && \
    echo 'deb http://security.debian.org/ stretch/updates main contrib non-free' >> /etc/apt/sources.list && \
    echo 'deb-src http://security.debian.org/ stretch/updates main contrib non-free' >> /etc/apt/sources.list

# Set stuff for steamcmd
RUN echo steamcmd steam/purge note | debconf-set-selections && \
    echo steamcmd steam/license note | debconf-set-selections && \
    echo steamcmd steam/question select I AGREE | debconf-set-selections

# Install Packages / Dependencies
RUN dpkg --add-architecture i386;\
    apt-get -y update; apt-get -y install steamcmd ca-certificates

# FIX ( perl: warning: Please check that your locale settings: )
# http://ubuntuforums.org/showthread.php?t=1346581
#RUN /usr/sbin/locale-gen en_US en_US.UTF-8 hu_HU hu_HU.UTF-8
#RUN dpkg-reconfigure locales

# # Cleanup
# RUN apt-get clean && \
#     rm -fr /var/lib/apt/lists/* && \
#     rm -fr /tmp/*

# Create user to run as
# script refuses to run in root, create user
# RUN groupadd -g 1000 -r csgoserver && \
# 	useradd -u 1000 -rm -g csgoserver csgoserver && \
# 	adduser csgoserver sudo && \
# 	echo "csgoserver ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
# USER csgoserver
WORKDIR /csgoserver

# Volume
# RUN chown -R csgoserver:csgoserver /home/csgoserver
# VOLUME ["/home/csgoserver/serverfiles"]

RUN echo '# CSGO SERVER INSTALL SCRIPT' > install && \
    echo '/usr/games/steamcmd +login anonymous +force_install_dir /csgoserver/server +app_update 740 validate +quit' >> install && \
    chmod +x install

# Edit Server Script to hold Docker Environmental Varables

# RUN cat csgoserver  # DEBUG

# Run Install Script
# RUN ./csgoserver auto-install

# Make Start Script
RUN echo '# Docker Start / Run Script' > start.sh && \
    echo '' >> start.sh && \
    echo '# Edit Server Config to hold Docker Environmental Varables' >> start.sh && \
    echo '# ------------------' >> start.sh && \
    echo 'if ! [ -f /csgoserver/server/csgo/cfg/server.cfg ]; then' >> start.sh && \
    echo '  echo "hostname $SERVER_NAME" >> /csgoserver/server/csgo/cfg/server.cfg' >> start.sh && \
    echo '  echo "rcon_password $RCON_PASS" >> /csgoserver/server/csgo/cfg/server.cfg' >> start.sh && \
    echo '  echo "sv_password $SERVER_PASS" >> /csgoserver/server/csgo/cfg/server.cfg' >> start.sh && \
    echo '  echo "sv_lan $SERVER_LAN" >> /csgoserver/server/csgo/cfg/server.cfg' >> start.sh && \
    echo '  echo "sv_region $SERVER_REGION" >> /csgoserver/server/csgo/cfg/server.cfg' >> start.sh && \
    echo 'fi' >> start.sh && \
    echo '# ------------------' >> start.sh && \
    echo '' >> start.sh && \
    echo '# Script Manager' >> start.sh && \
#     echo './csgoserver auto-install' >> start.sh && \
#     echo './csgoserver update' >> start.sh && \
#     echo './csgoserver details' >> start.sh && \
    echo '/csgoserver/server/srcds_run -game csgo -console -usercon -autoupdate -tickrate $TICK_RATE -maxplayers $MAX_PLAYERS +ip $GAME_IP +net_public_adr $PUBLIC_IP -port $GAME_PORT +clientport $CLIENT_PORT +hltv_port $SOURCE_TV_PORT +game_type $GAME_TYPE +game_mode $GAME_MODE +mapgroup $MAP_GROUP +map $DEFAULT_MAP  +sv_setsteamaccount $SERVER_TOKEN +exec server.cfg' >> start.sh && \
    chmod +x start.sh

# Run Start Script
# https://labs.ctl.io/dockerfile-entrypoint-vs-cmd/
# http://stackoverflow.com/questions/21553353/what-is-the-difference-between-cmd-and-entrypoint-in-a-dockerfile
# http://kimh.github.io/blog/en/docker/gotchas-in-writing-dockerfile-en/
# http://www.markbetz.net/2014/03/17/docker-run-startup-scripts-then-exit-to-a-shell/
# http://crosbymichael.com/dockerfile-best-practices.html
# https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/
# ENTRYPOINT ["./csgoserver"]  # does not work the way I want to.
# CMD ["/bin/bash", "-c", "set -e && /home/csgoserver/start.sh"]  # DOES NOT STAY RUNNING.
CMD bash -c 'exec /csgoserver/start.sh';'bash'
